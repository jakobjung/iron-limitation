# iron-limitation

This project consists of code, data and analysis plots which were created and used for the 
master's thesis of Jakob Jung between October 2018 and May 2019. The goal was to change a 
GEM to simulate iron limitation of _S. aureus_ N315.

3 main directories show the used data (data), scripts written (scripts) and
analysis tools (analysis). More information on these can be found in their
own README files. 