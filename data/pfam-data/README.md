# pfam-data
Data used to identify iron-related reactions of the GEM  of S. aureus N315 based on
iron-related pfam domains. 
## Iron_rel_pfams_rosato_22_12_2016_off.csv
I received the data file 'Iron_rel_pfams_rosato_22_12_2016_off.csv' from Antonio Rosato on 
(received: 22-November-2018) all iron related pfam domains. This data includes all known 
domains of different organisms and needed to be translated to the connected genes inside 
the GEM.
## query-result.csv
file, received by a query, which shows all pfams connected to S. aureus N315 as well
as the protein-id related to the respective domains. This was used to retrieve the pids
which can later be translated to gene-ids. 

Received by Maria Suarez Diez on the 26-November-2018. Before, the genome was downloaded from
the NCBI database and annotated using SAPP and the InterProScan module.
## sequence.gb
genebank file, containing all genes with gene-ids, PDB-names and protein ids for S.aureus
N315. A parser was written in python for this file.

Downloaded from: https://www.ncbi.nlm.nih.gov/nuccore/BA000018.3

Date: 01-December-2018
