# models-sa
In this directory, there are 3 subdirectories:
## initial-models-sa
64 models of 64 S. aureus strains from a study by Bosi et al. 2016. 
They are, as received from the authors of the study, in JSON format ().
## curated-models
Same 64 models, as modified by the python script  scripts/model_curation/add_missing_reactions.py
more information on the code can be found in the code scripts docstring
## missing-reactions
Supporting Information on the JSON models of the Bosi study, downloaded on the
29-Nov-2018 from https://www.pnas.org/content/113/26/E3801/tab-figures-data.
The excel file was firstly converted, sheets were separated and changed
to csv-format, for easier reading with python.

