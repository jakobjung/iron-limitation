# BRENDA-db
All the data which is used in the identification of iron-related enzymes using
the BRENDA database for queries.
## brenda_download.txt:
Downloaded from: https://www.brenda-enzymes.org/download_brenda_without_registration.php
; Date of download: 10-November-2018; 
Description: Text-file containing data of all enzymes including EC-numbers,
             protein IDs, Cofactors and more. More information can be found in 
             the README file on the website.
## all_ecs_from_model_validated.csv:
Iron related EC-numbers, which are both in the GEM model of S. aureus N315 
and in the BRENDA-database. They were identified to either have iron as a 
cofactor, metal ion, or require iron in any way to be able to catalyse the 
reaction. 
## brenda.db:
The file that was the output of the brenda parser (scripts/BRENDA-db-search/Brend-parser.py).
It is a sql database and can be used by programs like SQLite. 