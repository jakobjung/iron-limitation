"""
Title:      Sampling solution space in different iron-conditions and perform fva
Author:     Jakob Jung
Date:       07 January 2019
Details:
"""
import cobra
from cobra.flux_analysis import sample
from cobra.flux_analysis import flux_variability_analysis


saureus = cobra.io.load_json_model("../../data/models-sa/iron_adjusted_model/SAN315_with_iron.json")
all_conditions = [43]
with saureus:
    for cond in all_conditions:
        saureus.reactions.get_by_id('EX_Fe_LPAREN_e_RPAREN_').upper_bound = cond
        saureus.reactions.get_by_id('EX_Fe_LPAREN_e_RPAREN_').lower_bound = -cond
        f_sample = sample(saureus, 1000)
        f_fva = flux_variability_analysis(saureus)
        f_sample.to_csv("../../data/simulation_results/sample_conc_{}.csv".format(cond))
        f_fva.to_csv("../../data/simulation_results/FVA_conc_{}.csv".format(cond))
        print("Iron content {} - fba_Fe_Ox_limitation are done".format(cond))

