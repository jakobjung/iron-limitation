#Set working directory:
setwd("~/Documents/iron-limitation/scripts/flux_sampling")

#load necessary packages:
library(readr)
library(ggplot2)
library(matrixStats)
library(VennDiagram)


#Read the files of flux-sampling into memory (high, medium, low iron-content:
d_1000 <- read_csv("../../data/simulation_results/sample_conc_1000.csv")
d_70 <- read_csv("../../data/simulation_results/sample_conc_70.csv")
d_50 <- read_csv("../../data/simulation_results/sample_conc_50.csv")
d_49 <- read_csv("../../data/simulation_results/sample_conc_49.csv")
d_30 <- read_csv("../../data/simulation_results/sample_conc_30.csv")
d_10 <- read_csv("../../data/simulation_results/sample_conc_10.csv")

#inspect data:
d_30[1:5,1:5]

#Delete first column (just indexes (1-n))
d_1000[1] <- NULL
d_70[1] <- NULL 
d_50[1] <- NULL
d_49[1] <- NULL 
d_30[1] <- NULL 
d_10[1] <- NULL 

#normalize data (divide fluxes by glucose intake)
d_70 <- d_70/0.111
d_49 <- d_49/0.106
#d_1000 <- d_1000/0.111
#d_70 <- d_70 * (0.111/0.111)
#d_50 <- d_50/0.107
#d_49 <- d_49 * (0.111/0.106)
d_30 <- d_30/0.063
#d_10 <- d_10 *(0.111/0.015)
#d_70 <- d_70/8
#d_49 <- d_49/8
#d_30 <- d_30 /5


#transpose data:
td1000 <- data.frame(t(d_1000))
td70 <- data.frame(t(d_70))
td50 <- data.frame(t(d_50))
td49 <- data.frame(t(d_49))
td30 <- data.frame(t(d_30))
td10 <- data.frame(t(d_10))



#compare high & medium (diff-matrix), high&low and medium&low:
diff_1000_70 <- abs(td1000) - abs(td70)
diff_70_49 <- abs(td70) - abs(td49)
diff_70_30 <- abs(td70) - abs(td30)
diff_49_30 <- abs(td49) - abs(td30)

#get difference mean per reaction:
diff_1000_70$mean_diff <- rowMeans(diff_1000_70) 
diff_70_49$mean_diff <- rowMeans(diff_70_49) 
diff_70_30$mean_diff <- rowMeans(diff_70_30) 
diff_49_30$mean_diff <- rowMeans(diff_49_30) 

#get difference stdev per reaction:
diff_1000_70$stdev <- apply(diff_1000_70[,1:1000], 1, sd)
diff_70_49$stdev <- apply(diff_70_49[,1:1000], 1, sd)
diff_70_30$stdev <- apply(diff_70_30[,1:1000], 1, sd)
diff_49_30$stdev <- apply(diff_49_30[,1:1000], 1, sd)

#get the z-value |mean|/(stdev/sqrt(n)), where n = 1000 observations, 
diff_1000_70$zval <- abs(diff_1000_70$mean_diff)/(diff_1000_70$stdev / sqrt(1000))
diff_70_49$zval <- abs(diff_70_49$mean_diff)/(diff_70_49$stdev / sqrt(1000))
diff_70_30$zval <- abs(diff_70_30$mean_diff)/(diff_70_30$stdev / sqrt(1000))
diff_49_30$zval <- abs(diff_49_30$mean_diff)/(diff_49_30$stdev / sqrt(1000))

#convert nas to 0
diff_1000_70$zval[is.nan(diff_1000_70$zval)] <- 0
diff_70_49$zval[is.nan(diff_70_49$zval)] <- 0
diff_70_30$zval[is.nan(diff_70_30$zval)] <- 0
diff_49_30$zval[is.nan(diff_49_30$zval)] <- 0

#check for highest z-values
subset(diff_1000_70, (zval > 70)&(mean_diff<-0.1))[,1001:1003]
subset(diff_70_49, (zval > 70)&(mean_diff < -0.1))[,1001:1003] 
subset(diff_70_30, (zval > 70)&(mean_diff < -0.1))[,1001:1003] 
subset(diff_49_30, (zval > 70)&(mean_diff < -0.1))[,1001:1003] 

#get vectors of reactions with z-values higher than 60:
hm_high_zval <- rownames(diff_70_49)[(diff_70_49$zval > 20)&(abs(diff_70_49$mean_diff)>0.1)]
hl_high_zval <- rownames(diff_70_30)[(diff_70_30$zval > 20)&(abs(diff_70_30$mean_diff)>0.1)]
ml_high_zval <- rownames(diff_49_30)[(diff_49_30$zval > 20)&(abs(diff_49_30$mean_diff)>0.1)]

#Check venn-diagram, print data:
dir.create("../../analysis/sampling/venn_diagram")
vd <- list(high_medium = hm_high_zval, high_low = hl_high_zval, medium_low = ml_high_zval)
venn.diagram(vd, filename = "../../analysis/sampling/venn_diagram/venn_z_30_norm", fill = c("blue", "red", "green"))
venn_overlap <- calculate.overlap(vd)
print(venn_overlap)

reaction <- 'PPM'
ggplot(d_70) + geom_density(aes(x=d_70[,reaction], color='high'))+ geom_density(aes(x=d_49[,reaction], color='medium')) +
  geom_density(aes(x=d_30[,reaction], color='low')) #+ xlim(-60, 0)#+ geom_density(aes(x=d_10$EX_rib_D_LPAREN_e_RPAREN_, color='10'))


#create lists of reactions' pathways
glycolysis <- c('PPCK','ACLS_c','PYK','PGK','GAPDy','GLCBPP','ALDD2x','GAPD_fw','GAPD_rv','LDH_L_fw','LDH_L_rv',
                'TPI','ACS','PGM','ENO','ALCD2x_fw','ALCD2x_rv','G6PP', '6PHBG2','PGM','ENO','PGI','PFK', 'FBA')

#Create directory for storing the graphs:
dir.create("../../analysis/sampling")
dir.create("../../analysis/sampling/glycolysis")

# make graphs for each reaction and save them in jpeg-file:
for (reaction in glycolysis) {
  jpeg(sprintf("../../analysis/sampling/glycolysis/%s_norm.jpeg", reaction), width = 900, height = 700)
  print(ggplot(d_70) + geom_density(aes(x=d_70[[reaction]], color='high'))+ 
          geom_density(aes(x=d_49[[reaction]], color='medium')) +
          geom_density(aes(x=d_30[[reaction]], color='low') )+ labs(title = reaction, x = "flux (mmol/gDW/hr)")+
          theme(text = element_text(size=30)))
  dev.off()
}

# same for tca:
tca <- c('PPCK', 'CS', 'ACONTa_fw','ACONTa_rv', 'ICITRED', '2OXOTHP', 'FUM', 'ACONTb_fw','ACONTb_rv', 
         'OSUCCL','PC', 'SUCOAS', 'ICDHyr', 'CITL', 'AKGDH', 'SUCD1','MDH3', 'PDH' )
dir.create("../../analysis/sampling/TCA_cycle")

# make graphs for each reaction and save them in jpeg-file:
for (reaction in tca) {
  jpeg(sprintf("../../analysis/sampling/TCA_cycle/%s_norm.jpeg", reaction), width = 900, height = 700)
  print(ggplot(d_70) + geom_density(aes(x=d_70[[reaction]], color='high'))+ 
          geom_density(aes(x=d_49[[reaction]], color='medium')) +
          geom_density(aes(x=d_30[[reaction]], color='low') )+ labs(title = reaction, x = "flux (mmol/gDW/hr)")+
          theme(text = element_text(size=30)))
  dev.off()
}

# same for PPP:
ppp <- c('GND', 'DRPAr', 'G6PBDH', 'GNK', 'PPM2', 'PGLer', 'RPE_fw','RPE_rv', 'PRPPS', 
         'EX_pi_LPAREN_e_RPAREN_', 'SAS40', 'RBK', 'DDGLK', 'EDA', 'EX_rib_D_LPAREN_e_RPAREN_', 'RPI')
dir.create("../../analysis/sampling/PPP")
for (reaction in ppp) {
  jpeg(sprintf("../../analysis/sampling/PPP/%s_norm.jpeg", reaction), width = 900, height = 700)
  print(ggplot(d_70) + geom_density(aes(x=d_70[[reaction]], color='high'))+ 
          geom_density(aes(x=d_49[[reaction]], color='medium')) +
          geom_density(aes(x=d_30[[reaction]], color='low') )+ labs(title = reaction, x = "flux (mmol/gDW/hr)")+
          theme(text = element_text(size=30)))
  dev.off()
}
# same for complete hm_ml:
venn_hm_ml <- venn_overlap$a4
dir.create("../../analysis/sampling/venn_hm_ml")
for (reaction in venn_hm_ml) {
  jpeg(sprintf("../../analysis/sampling/venn_hm_ml/%s_norm.jpeg", reaction), width = 900, height = 700)
  print(ggplot(d_70) + geom_density(aes(x=d_70[[reaction]], color='high'))+ 
          geom_density(aes(x=d_49[[reaction]], color='medium')) +
          geom_density(aes(x=d_30[[reaction]], color='low') )+ labs(title = reaction, x = "flux (mmol/gDW/hr)")+
          theme(text = element_text(size=30)))
  dev.off()
}

# same for complete overlap
complete_overlap <- venn_overlap$a5
dir.create("../../analysis/sampling/complete_overlap")
for (reaction in complete_overlap) {
  jpeg(sprintf("../../analysis/sampling/complete_overlap/%s_norm.jpeg", reaction), width = 900, height = 700)
  print(ggplot(d_70) + geom_density(aes(x=d_70[[reaction]], color='high'))+ 
          geom_density(aes(x=d_49[[reaction]], color='medium')) +
          geom_density(aes(x=d_30[[reaction]], color='low') )+ labs(title = reaction, x = "flux (mmol/gDW/hr)")+
          theme(text = element_text(size=30)))
  dev.off()
}

#same for 
pur = c('PUNP1', 'ATPM', 'RNDR1', 'ADK1_fw', 'PRASCSi', 'PUNP4', 'GNNUC', 'PUNP5', 'PRFGS', 'AICART', 'IMPD', 
        '23PDE9pp', 'XPPT', 'DGK1_fw', 'ADPT', 'NDPK5_fw', 'PUNP7', 'PYK', 'ADSS', 'ADNCYC', 'GLUPRT', 'RNTR1',
        'RNTR2', 'NDPK1_fw', 'NDPK9', 'AIRC', 'IMPC', 'ADSL1', 'NTD6pp', 'PUNP6', 'INSH','23PDE7pp', 'HXPRT',
        'GTPDPK', 'PUNP3', 'PRPPS', 'ADNUC', 'PRAIS', 'DADNK', 'GUAPRT', 'PUNP2', 'DADK_fw', 'NDPK8_fw', 'PRAGS',
        'XTSNH', 'ADSL2r', 'GK1_fw', 'DGNSK', 'GMPS2', 'RNDR2', 'GARFT_fw', 'NTPP2', 'NTPP9', 'NTPP11', 'NTPP10',
        'ADKd_fw', 'ADSK', 'AIRC2', 'AIRC3', 'PRASCS', 'UPPRTr', '3NTD9pp', 'DADK_rv', 'ADK1_rv', 'GARFT_rv', 
        'GK1_rv', 'NDPK8_rv', 'NDPK5_rv', 'DGK1_rv', 'NDPK1_rv', 'ADKd_rv', 'PRFGS', 'AIRC4')
dir.create("../../analysis/sampling/purine_metabolism")
for (reaction in pur) {
  jpeg(sprintf("../../analysis/sampling/purine_metabolism/%s_norm.jpeg", reaction), width = 900, height = 700)
  print(ggplot(d_70) + geom_density(aes(x=d_70[[reaction]], color='high'))+ 
          geom_density(aes(x=d_49[[reaction]], color='medium')) +
          geom_density(aes(x=d_30[[reaction]], color='low') )+ labs(title = reaction, x = "flux (mmol/gDW/hr)")+
          theme(text = element_text(size=30)))
  dev.off()
}



pyr = c('NDPK6_fw', 'CYTK1', 'PYNP1', 'UCPP', 'DCPP', 'URIK2', 'DTUPP', 'NDPK4_fw', '23PDE2pp', 'URIK3', 'DURIK1',
        'DAUPP', 'DGUPP', 'ASPCT', 'NDPK3_fw', 'URIDK2r', 'DUCYTP', 'NTD4pp', 'DURIPP', 'CYTDK1', 'RNTR4', 'CYTK2',
        'DTMPK_fw', 'CTPS2', 'CYTDK2', 'UMPK', 'NDPK2_fw', 'UDCTPP', 'NDPK7_fw', 'CDDTPP_fw', 'UUPP', 'UDUPP',
        'DUUPP', 'NTD1', 'TMDK1', 'OMPDC', 'DACTPP', 'RNDR3', '23PDE4pp', 'DHORTS_fw', 'RNDR4', 'CBPS', 'URIK1',
        'RNTR3', 'ORPT', 'PYNP2r', 'DUTPDP', 'NTPP8', 'TMDS', 'ADKd_fw', 'AIRC2', 'AIRC3', 'PRASCS', 'UPPRTr', 
        'TMDPP', 'NDPK7_rv', 'DTMPK_rv', 'NDPK2_rv', 'NDPK4_rv', 'CDDTPP_rv', 'NDPK3_rv', 'NDPK6_rv', 'ADKd_rv', 
        'DHORTS_rv')
dir.create("../../analysis/sampling/pyrimidine_metabolism")
for (reaction in pyr) {
  jpeg(sprintf("../../analysis/sampling/pyrimidine_metabolism/%s_norm.jpeg", reaction), width = 900, height = 700)
  print(ggplot(d_70) + geom_density(aes(x=d_70[[reaction]], color='high'))+ 
          geom_density(aes(x=d_49[[reaction]], color='medium')) +
          geom_density(aes(x=d_30[[reaction]], color='low') )+ labs(title = reaction, x = "flux (mmol/gDW/hr)")+
          theme(text = element_text(size=30)))
  dev.off()
}






Num_Reactions_Model=1380

Num_Reactions_Change=12

Num_Reactions_inPath=14 
Num_Reactions_inPath_Change=4

1-phyper(Num_Reactions_inPath_Change, Num_Reactions_inPath, Num_Reactions_Model-Num_Reactions_inPath,Num_Reactions_Change)




# for report:
ggplot(d_70) + geom_density(aes(x=PFK, color='70'))+ geom_density(aes(x=d_49$PFK, color='49'))+ xlim(0, 30)


