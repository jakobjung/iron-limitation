# flux sampling
Scripts, which were used to run and analyse the results of flux sampling of the 
solution space.
## sampling.py
script that runs flux-sampling.

## fluxsampling_nb.ipynb
python notebook, used to analyse the results of flux-sampling. 


## sampling_analysis.r 
R script, used to analyse the results of flux-sampling. 