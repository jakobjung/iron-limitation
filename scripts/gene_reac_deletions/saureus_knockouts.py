"""
Title:      Performing knock-outs in different iron-conditions
Author:     Jakob Jung
Date:       07 January 2019
Details:    Simulates single gene deletions under different iron-conditions.

"""
import cobra
from cobra.flux_analysis import (single_gene_deletion,
                                 single_reaction_deletion,
                                 double_gene_deletion,
                                 double_reaction_deletion)


def single_grd(model, iron_concentrations, treshold = 0.01):
    essential_gene_dict = {}
    essential_reaction_dict = {}
    for conc in iron_concentrations:
        with model:
            med = model.reactions.get_by_id('EX_Fe_LPAREN_e_RPAREN_')
            med.upper_bound = conc
            med.lower_bound = conc
            opt = model.slim_optimize()
            sgd = single_gene_deletion(model)
            srd = single_reaction_deletion(model)
            essential_genes = []
            for index, row in sgd.iterrows():
                if row['growth'] < opt*treshold:     # find out how many genes cause less than 70% max growth.
                    essential_genes += [str(x) for x in row.name]
            essential_reactions = []
            for index, row in srd.iterrows():
                if row['growth'] < opt*treshold:     # find out how many reactions cause less than 70% max growth.
                    essential_reactions += [str(x) for x in row.name]
            print(opt)
            print("ess.reactions with {}: ".format(conc), len(essential_reactions))
            print("ess.genes with {}: ".format(conc), len(essential_genes))
            essential_gene_dict[conc] = essential_genes
            essential_reaction_dict[conc] = essential_reactions
    return essential_gene_dict, essential_reaction_dict


def double_grd(model, iron_concentrations):
    for conc in iron_concentrations:
        with model:
            print("Double deletions ({}):".format(conc))
            med = model.reactions.get_by_id('EX_Fe_LPAREN_e_RPAREN_')
            med.upper_bound = conc

            dgd = double_gene_deletion(model, model.genes[:100], return_frame=True).round(4)
            essential_genes = []
            essential_reactions = []
            for index, row in dgd.iterrows():
                if row['growth'] < 0.01:
                    essential_genes += [[str(x) for x in row.name]]
            print("Double gd: ", len(essential_genes))
            drd = double_reaction_deletion(model, model.reactions[:100], return_frame=True).round(4)
            for index, row in drd.iterrows():
                if row['growth'] < 0.01:
                    essential_reactions += [[str(x) for x in row.name]]
            print("Double rd: ", len(essential_reactions))


if __name__ == '__main__':
    saureus = cobra.io.load_json_model("../../data/models-sa/iron_adjusted_model/SAN315_with_iron.json")
    essential_gene_dict, essential_reaction_dict = single_grd(saureus, [70, 49, 30], 0.7)
    # double_grd(saureus, [70, 49, 30])
    for r in essential_reaction_dict[30]:
        if r not in essential_reaction_dict[70]:
            print(r, saureus.reactions.get_by_id(r).name, saureus.reactions.get_by_id(r).genes,
                  saureus.reactions.get_by_id(r).subsystem)
