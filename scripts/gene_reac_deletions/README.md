# Gene and reaction deletions
## saureus.knockouts.py
simulates single gene- and reaction deletions. Helps to determine essential genes and
reactions. Also it was used to look for reaction-cruciality.