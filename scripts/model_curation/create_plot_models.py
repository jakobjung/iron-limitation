"""
Title:      Creates plot for earlier models of S.aureus N315
Author:     Jakob Jung
Date:       11 March 2019
Details:    This script creates a nice plot for the report and saves it in the analysis folder.
"""

import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

figure(num=None, figsize=(11, 6))

publications = ['Becker et al. 2005', 'Heinemann et al. 2005','Lee et al. 2009', 'Bosi et al. 2016', 'Our GEM 2019']
reaction_numbers = [640, 774, 1497, 1394, 1380]
plt.style.use('ggplot')
plt.bar(publications, reaction_numbers, width=0.3)
plt.ylabel('Number of reactions', fontsize=12)
plt.show()

plt.savefig("../../analysis/report_graphics/figure_publications.png")
