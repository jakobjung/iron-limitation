"""
Title:      adding missing reactions to the CBM of 64 SA models
Author:     Jakob Jung
Date:       12 December 2018
Details:    This script takes all 64 GEMs (json files) and adds the missing
            reactions to the models. All original reactions have been taken
            from the excel sheet provided by Bosi et al. 2016 in suppl. materials.
            Now all models are complete. Even the GPR rules are added for
            the non-exchange reactions (mostly exchange-reactions were missing,
            which do not have genes assigned). Another feature of the second
            function 'remove_dupl_reacs' is that it removes duplicate reactions.
"""
import csv
from cobra import Reaction,Metabolite,Model
import cobra
import re
import os


def add_missing_reactions_allfiles(pathmodels, pathcsv, pathdest):
    """adds missing reactions to models and adds them to defined destination

    :param pathmodels: path to the file which includes all 64 S. aureus models
                       in json format; string
    :param pathcsv: csv-file, which contains all reactions for all 64 strains;
                    string
    :param pathdest: path to destination directory, where the updated models
                     will be saved; string
    :return: set of all added reactions (not redundant); set
    details:        First it adds missing reactions to the model and then uses
                    the function remove_dupl_reacs to filter out all duplicate
                    reactions inside the model.
    """
    model_list = os.listdir(pathmodels)  # make a list of all model_names ("... .json")
    added_reactions = []                 # for testing
    for model in model_list:    # go through all models in file
        m_name = model[:-5]     # remove the '.json' from filename
        model = cobra.io.load_json_model(pathmodels + "/" + model)  # load model
        print(model.id)
        print(len(model.reactions))
        with open(pathcsv, newline = '') as a_r:            # open csv-file from study
            reader = csv.reader(a_r)
            row1 = next(reader)
            m_index = row1.index(m_name)
            for row in reader:
                # checks if reaction is in real model[1], and compares to the initial model:
                if row[m_index] == '1' and row[0] not in model.reactions:
                    added_reactions += [row[0]]     # shows which reacs were missing
                    reaction = Reaction(row[0])     # creates reaction
                    reaction.name = row[1]          # creates name for reaction
                    reaction.lower_bound = int(row[3])  # creates bounds
                    reaction.upper_bound = int(row[4])
                    # Creates notes section as dictionary:
                    notes = {}
                    for i in row[5].split("|"):
                        i = i.split(":")
                        notes[i[0]] = i[1]
                    reaction.notes = notes
                    # check non-exchange reactions and add GPRs from GPR-file:
                    if not reaction.id.startswith("EX"):
                        p = "../../data/models-sa/missing-reactions/GPR_sheet.csv"
                        with open(p, newline='') as f:
                            r = csv.reader(f)
                            r1 = next(r)
                            m_i = r1.index(model.id)
                            for row1 in r:
                                if row1[0] == reaction.id:
                                    reaction.gene_reaction_rule = row1[m_i]
                    # now the reaction can be added:
                    model.add_reactions([reaction])
                    # reaction (string representation can only be added after the reaction
                    # being added to the model:
                    model.reactions.get_by_id(row[0]).reaction = row[2]

        # print(len(added_reactions), len(set(added_reactions)))
        print(len(model.reactions))
        diff = 1
        while diff > 0:     # might have to run it more than once, since triplicates can be present
            bef = len(model.reactions)
            remove_dupl_reacs(model)
            af = len(model.reactions)
            diff = bef - af
        print(len(model.reactions))
        adjust_metabolite_charge(model)
        adjust_reaction_ids(model)
        cobra.io.save_json_model(model, pathdest + "/" + m_name + ".json")   # add model to destination
    return set(added_reactions)     # returns all reactions that are modified in at least 1 model (not necessary)


def remove_dupl_reacs(model):
    """Removes reactions with exactly same function from GEM.

    :param model: Metabolic model which is previously downloaded and read (obj)
    :return:      Modified model
    Details:    A regex is used to check for same metabolites and stoch. balance
                of reactions. Duplicate reactions are deleted, based on several
                factors:
                1:  if there's one reversible and one irreversible reaction,
                    the irreversible is being kept
                2:  if the reaction bounds differ, the reaction with bigger bounds
                    and thus the less constrained reaction  wil be deleted. Exception:
                    if one reaction has 0 and 0 as bounds, it is deleted (not active).
                3:  if the length of notes (additional information) differs, the
                    reaction with less information will be deleted.
    """
    matcher = re.compile("<.*?>: -?[\d]")   # for matching strings of metabolite data
    react_dict = {}
    met = {}   # initialize dict with metabolites str as key and r.id as value
    for r in model.reactions:      # parse through all reactions of model
        # check if reaction is duplicate
        if str(matcher.findall(str(r.metabolites))) in met.keys():
            r1 = r     # give name to both duplicate reactions
            r2 = model.reactions.get_by_id(met[str(matcher.findall(str(r.metabolites)))])
            if "<=>" in r1.reaction and "<=>" not in r2.reaction:
                r2.delete()
            elif "<=>" in r2.reaction and "<=>" not in r1.reaction:
                r1.delete()
            elif r1.bounds != r2.bounds:          # choose the react. with more constrained bounds
                if r1.lower_bound < r2.lower_bound < 0 or r1.upper_bound > r2.upper_bound > 0\
                        or r1.bounds == (0, 0):
                    r1.delete()
                else:
                    r2.delete()
            elif len(r1.notes) < len(r2.notes):     # choose reaction with more information
                r1.delete()
            else:
                r2.delete()     # If previous ones are identical, delete the latter reaction
            continue            # continue, to prevent calling deleted reactions in the following

        met[str(matcher.findall(str(r.metabolites)))] = r.id
        react_dict[r.reaction] = r.id
    return model


def adjust_reaction_ids (model):
    """Adjusts the model to remove '-' from reaction ids (not allowed by validator, add '_')

    :param model: Metabolic model which is previously downloaded and read (obj)
    :return: model without a '-' in its reaction ids
    """
    for r in model.reactions:
        if '-' in r.id:
            r.id = r.id.replace("-", "_")
    return model


def adjust_metabolite_charge(model):
    """Adjusts the model to remove none-charge from metabolite charge (not allowed by validator)

        :param model: Metabolic model which is previously downloaded and read (obj)
        :return: model without a '-' in its reaction ids
        """
    for m in model.metabolites:
        if type(m.charge) is None:
            m.charge = 0

        elif type(m.charge) == float:
            m.charge = int(m.charge)
    return model







# if __name__ == '__main__':
#     pm = "../../data/models-sa/initial-models-sa"
#     pcsv = "../../data/models-sa/missing-reactions/all_reacs_all_models.csv"
#     pd = "../../data/models-sa/curated-models"
#     add_missing_reactions_allfiles(pm, pcsv, pd)
#     saureus = cobra.io.load_json_model("../../data/models-sa/curated-models/\
# SA_N315_uid57837_158879_1.json")




