"""
Title:      specifically curates model of S.aureus N315
Author:     Jakob Jung
Date:       22 January 2019
Details:    This script takes the S. aureus N315 model and deletes several reactions which have the same
            name. If one of the duplicate reactions is not balanced out, it is deleted. this script also
            changes other reactions, which are manually found to be wrong. This script should be run
            before  "curate_N315_spec.py"!

"""

from cobra import Reaction,Metabolite,Model
import cobra
import collections
import os

# import the model:
saureus = cobra.io.load_json_model("../../data/models-sa/curated-models/SA_N315_uid57837_158879_1.json")
# find all reactions which might be duplicate due to same name:
reacs = [r.name for r in saureus.reactions]
dupl_reacs = [item for item, count in collections.Counter(reacs).items() if count > 1]


# Now, all of these reactions are checked manually to see whether one needs to be deleted.
# these ones are deleted:
saureus.reactions.get_by_id("GPDDA4pp").delete()  # is not balanced!
saureus.reactions.get_by_id("GPDDA1pp").delete()  # is not balanced!
saureus.reactions.get_by_id("PTPAT").delete()  # is not balanced!
saureus.reactions.get_by_id("GPDDA2pp").delete()  # is not balanced!
saureus.reactions.get_by_id("HACD7").delete()  # reverse reaction, actually the same with HACD7i
saureus.reactions.get_by_id("AGPRi").delete()    # is not balanced!

# add compartment to missing reactions
for r in saureus.metabolites:
    if r.compartment == "":
        r.compartment = "{}".format(r.id[-1])


# a reaction of glycolysis pathway has been found, which is in the wrong direction (pyruvate kinase):
saureus.reactions.get_by_id("PYK").upper_bound = 0
saureus.reactions.get_by_id("PYK").lower_bound = -1000
saureus.reactions.get_by_id("PYK").subsystem = "Glycolysis / Gluconeogenesis; Purine metabolism; Pyruvate metabolism"

# other manual curations (pathways):
saureus.reactions.get_by_id("RBKr").delete()
saureus.reactions.get_by_id("PRPPS").subsystem = "Pentose phosphate pathway; Purine metabolism"
saureus.reactions.get_by_id("ADK1").subsystem = "Purine metabolism"
saureus.reactions.get_by_id("RBK").subsystem = "Pentose phosphate pathway"
saureus.reactions.get_by_id("EX_pi_LPAREN_e_RPAREN_").subsystem = "Pentose phosphate pathway"
saureus.reactions.get_by_id("EX_rib_D_LPAREN_e_RPAREN_").subsystem = "Pentose phosphate pathway"
saureus.reactions.get_by_id("CTPS1").subsystem = "Pyrimidine metabolism"
saureus.reactions.get_by_id("PGI").subsystem = "Glycolysis"
saureus.reactions.get_by_id("RPI").subsystem = "Pentose phosphate pathway"


cobra.io.save_json_model(saureus, "../../data/models-sa/final_model_N315/saureus_N315.json")
cobra.io.write_sbml_model(saureus, "../../data/models-sa/final_model_N315/saureus_N315.xml")

