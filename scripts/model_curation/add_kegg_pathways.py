"""
Title:      adding pathways to the CBM of 64 SA models
Author:     Jakob Jung
Date:       31 January 2019
Details:    Python 2 has to be used for this script!
            Adapted from https://gitlab.com/wurssb/kegg_plotter/blob/master/crawler.py
"""


import sys
from urllib.request import urlopen
import urllib3
import time
import os
from requests import get

path = "../../data/KEGG_pathways/br08901.keg"
list_pathwayMaps = [] # list to extract for KEGG crawl

for line in open(path).readlines():
    if line.startswith('C'):
        sline = line.strip()
        spline = sline.split()
        pathway = spline[1]
        pathwayMap = 'ec'+str(pathway)
        list_pathwayMaps += [pathwayMap]

# Do the actual crawling
sPath = "../../data/KEGG_pathways/"
for pathwayMap in list_pathwayMaps:
    print(pathwayMap)
    try:
        url='http://rest.kegg.jp/get/'+pathwayMap+'/kgml'
        information_KEGG = urllib2.urlopen(url)
        dest = open(sPath+str(pathwayMap)+'.xml', 'w')
        dest.write(information_KEGG.read())
        time.sleep(0.5)
        dest.close()
    except:
        print ('Could not fetch information for '+pathwayMap)
