# model_curation
##add_missing_reactions.py
Here, one script can be found, which can be used in order to add all missing
reactions to the 64 initial JSON models of _S. aureus_ strains. more information
can be found in the docstrings of 'add_missing_reactions.py'

##curate_N315_spec.py
Here, a script can be found which takes the S. aureus N315 model and deletes several 
reactions which have the same name. If one of the duplicate reactions is not balanced out,
it is deleted.

##add_kegg_pathways.py
This script has to be run using python 2.7 and would not work with python 3! 
It downloads pathway htext-files of :
http://www.genome.jp/kegg-bin/get_htext?htext=br08901.keg (done on 08-Feb-2019)
 to the desired file path.

##get_ec_rxn_kegg.py
This script reads in the files downloaded by "add_kegg_pathways.py" and collects the ec-numbers and 
KEGG-ids  of every reaction in the pathway. Then it parses through all reactions of the S.aureus N315
model and annotates their pathways (subset) based on KEGG-data. 

##create_plot_models.py
This script creates a plot of the number of reactions of previously published GEMs
of S. aureus N315 and our GEM (curated GEM before iron addition). We used the plot 
in the final report to visualize the chronological changes. 