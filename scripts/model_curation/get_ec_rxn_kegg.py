"""
Title:      Adding subsets to the reactions of the SA N315 model
Author:     Jakob Jung
Date:       4 February 2019
Details:    This script takes a directory (in this case, fetched_PWS_SAN315) and the model of SN315.
            Then it adds pathway names to reactions, if they contain the related KEGG-id in the notes section.
            the pathway names are added to the "subsystem" attribute of the reactions.
"""

import re
import os
import cobra


def find_pathway_name(filepath):
    """Returns the pathway name of a previously downloaded xml-file from kegg representing a pathway.

    :param filepath:    path to the respective file
    :return:            name of pathway of file
    """
    re_pathway_name = re.compile(r'title="(.*)"')
    xml_file = open(filepath, "r")
    pathway_name = ""
    for line in xml_file:
        match_name = re.search(re_pathway_name, line)
        if match_name is not None:
            pathway_name = match_name.group(1)
            break
    xml_file.close()
    return pathway_name


def get_ecs_rxns (file_path):
    """ Returns list of ec-nrs and kegg-ids of previously downloaded xml-file from kegg representing a pathway.

    :param file_path:   path to the respective file
    :return:            List, represented like [[keggid1, ec1],[keggid2,ec2a,ec2b]]. Note that more  than 1
                        ec-numbers might be present per reaction.
    """
    kegg_ec_lol = []
    re_ec_number = re.compile(r'ec:(\d{1,2}(?:\.(?:\-|\d{1,3})){3})')
    re_kegg_id = re.compile(r'rn:(R\d{5})')
    xml_file = open(file_path, "r")
    for line in xml_file:
        match_ec = re.findall(re_ec_number, line)
        match_kegg = re.findall(re_kegg_id, line)
        if match_ec != [] and match_kegg != []:
            kegg_ec_lol += [match_kegg + match_ec]
    return kegg_ec_lol


if __name__ == '__main__':
    path = "../../data/KEGG_pathways/fetched_pws_san315/"
    pathways_file_list = os.listdir(path)
    ec_dict = {}
    for pathway in pathways_file_list:
        file = path + pathway
        pathway_name = find_pathway_name(file)
        pathway_ecs_kegg = get_ecs_rxns(file)
        ec_dict[pathway_name] = pathway_ecs_kegg

    saureus = cobra.io.load_json_model("../../data/models-sa/final_model_N315/saureus_N315.json")

    kegg = re.compile(r'R\d{5}')
    ec = re.compile(r'\d{1,2}(?:\.(?:\-|\d{1,3})){3}')
    for r in saureus.reactions:     # go through all reactions
        for p in ec_dict:           # go through all pathways p
            for reac in ec_dict[p]:  # go through all reactions in pathway
                for e_r in reac:     # go through each field of [KEGG, EC]
                    try:               # find all matching kegg-ids

                        if e_r in re.findall(kegg, r.notes['kegg']):    # check if kegg-nr is in reaction
                            if p not in r.subsystem:    # check if kegg-nr is not yet annotated
                                if len(r.subsystem) > 0 and r.subsystem != "None":  # check if its first pathway
                                    r.subsystem = r.subsystem + '; ' + p
                                else:
                                    r.subsystem = p

                    except KeyError:    # means, that the reaction is not annotated with kegg id.
                        continue

    cobra.io.save_json_model(saureus, "../../data/models-sa/final_model_N315/saureus_N315.json")











