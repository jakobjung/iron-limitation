"""
Title:      Loading and inspecting the S.aureus metabolic model
Author:     Jakob Jung
Date:       28 August 2018
Details:    Functions created to get a quick overview of metabolic models.
            Also functions are shown, which can be applied to create plots
            of limitation of medium compounds (e.g. iron, glucose).

"""
import cobra
import matplotlib.pyplot as plt


def summ_model(model):
    """Summarizes main parts of the metabolic model as printed output.

    :param model: Metabolic model which is previously downloaded and read
    :output: prints summary of metabolic model (name, number of reactions,
             metabolites, genes)
    """
    print("Name: %s \nReactions: %s \nMetabolites: %s \nGenes: %s" %
          (model.id, len(model.reactions), len(model.metabolites),
           len(model.genes)))


def modify_medium(model, compound, newval = 0):
    """returns objective value using modified medium composition.

    :param model: Metabolic model of wanted organism (class)
    :param compound: ID of medium-compound to be modified (str).
    :param newval: New amount of compount (int). by default 0 (deletion)
    :return: Objective value using modified medium composition (float)
    """

    print('Obj. value before modification: %f'%(model.slim_optimize()))
    with model:
        medium = model.medium
        medium[compound] = newval
        model.medium = medium
        print('Obj. value after modification: %f'%(model.slim_optimize()))


def usual_iron_limitation (startconc, steps, model):
    """Plots optim. biomass while reducing iron content in wanted steps until 0
    :param steps: defines steps until biomass is 0
    :param model: Metabolic model of wanted organism (class)
    :param startconc: Starting concentration of iron in medium
    :Output: plot of biomass production (y) relatde to iron content in medium (x)
    """
    iron = startconc
    growth_rates = []
    iron_content = []
    while iron >= 0:
        iron_content += [iron]
        with model:
            medium = model.medium
            medium['EX_fe2_LPAREN_e_RPAREN_'] = iron
            medium['EX_fe3_LPAREN_e_RPAREN_'] = iron
            model.medium = medium
            growth_rates += [model.slim_optimize()]
        iron -= steps
    plt.style.use('ggplot')
    plt.plot(iron_content,growth_rates, linestyle = '--')
    plt.ylabel('Biomass growth rate (h$^{-1}$)')
    plt.xlabel('Iron uptake (mmol$\cdot$ g$_{DW}^{-1}\cdot$ h$^{-1}$)')
    plt.xlim(0.2,0)
    plt.legend(["Initial GEM"])


def new_iron_limitation(startconc, steps, model):
    iron = startconc
    growth_rates = []
    iron_content = []
    while iron >= 0:
        iron_content += [iron]
        with model:
            med = model.reactions.get_by_id('EX_Fe_LPAREN_e_RPAREN_')
            med.upper_bound = iron
            growth_rates += [model.slim_optimize()]
        iron -= steps
    plt.style.use('ggplot')
    plt.plot(iron_content, growth_rates)
    plt.ylabel('Biomass growth rate (h$^{-1}$)')
    plt.xlabel('Iron uptake (mmol$\cdot$ g$_{DW}^{-1}\cdot$ h$^{-1}$)')
    plt.xlim(startconc, 0)
    plt.legend(["Initial GEM", "Iron-adjusted GEM"])


def limitation (model, compound, name_compound, my_legend = ['-'], num_steps = 100, line_style = '-', z_order = 10):
    """Plots optim. biomass while reducing a component's content in wanted steps until 0
    :param compound: ID-Name of the compound to be lmited in simulation (string)
    :param model: Metabolic model of wanted organism (class)
    :param name_compound: Real name of compound (e.g. glucose
                                                for 'EX_glc_LPAREN_e_RPAREN_')(string)
    :Output: plot of biomass production (y) related to compound content in medium (x)
    """
    conc = model.medium[compound]
    steps = conc / num_steps
    growth_rates = []
    comp_content = []
    while conc >= 0:
        comp_content += [conc]
        with model:
            medium = model.medium
            medium[compound] = conc
            model.medium = medium
            growth_rates += [model.slim_optimize()]
        conc -= steps
    plt.style.use('ggplot')
    plt.plot(comp_content, growth_rates, linestyle = line_style, zorder=z_order)
    plt.ylabel('Biomass growth rate (h$^{-1}$)') #, fontsize=20)
    plt.xlabel('%s uptake (mmol$\cdot$ g$_{DW}^{-1}\cdot$ h$^{-1}$)' % name_compound)  # fontsize=20) can be changed
    if my_legend != ['-']:
        plt.legend(my_legend)
    # limitation in medium for S. aureus N315'%(name_compound)
    plt.xlim(1.2, 0)


if __name__ == '__main__':
    saureus_old = cobra.io.load_json_model("../../data/models-sa/initial-models-sa/SA_N315_uid57837_158879_1.json")
    saureus = cobra.io.load_json_model("../../data/models-sa/iron_adjusted_model/\
SAN315_with_iron.json")
    # Create a plot difference in oxygen-limitation between old and new GEM:
    # plot ox-limitation before modification:
    limitation(saureus_old, 'EX_o2_LPAREN_e_RPAREN_', 'Oxygen', line_style='--')
    # plot ox-limitation after modification (knock out PO2O before):
    with saureus:
        saureus.reactions.PO2OR.lower_bound = 0
        limitation(saureus, 'EX_o2_LPAREN_e_RPAREN_', 'Oxygen', num_steps=100, z_order=2,
                   my_legend=['Initial GEM', 'Modified GEM'])
        plt.savefig("../../analysis/report_graphics/figure_oxygen_limitation.png")

    # iron limitation, initial model:
    plt.figure()
    usual_iron_limitation(70, 1, saureus)
    # iron limitation new model:
    new_iron_limitation(70, 0.1, saureus)
    plt.savefig("../../analysis/report_graphics/figure_iron_limitation.png")



