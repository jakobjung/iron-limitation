# fba of iron and oxygen limitation
in this directory, scripts for several simulations using the COBRApy package 
and the model for _S. aureus_ N315 can be found. 
## plots_limitation
This script has several functions, which use limitation of metabolites (e.g. 
iron) and optimize biomass function. With this limitation data, graphics were
created using the matplotlib-package. 