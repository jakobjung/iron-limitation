# scripts
Here, all scripts of the project can be found. each sub-directory has its own
README file. 
##Conventions
Each of the python scripts is written in  Python 3.7.0 (default, Jun 28 2018, 
13:15:42). The COBRApy package was downloaded from the website
https://github.com/opencobra/cobrapy on 01-November-2018.
