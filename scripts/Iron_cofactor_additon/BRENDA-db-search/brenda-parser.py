#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script to parse the BRENDA flat file download.

See also README.txt that can be found for download on the BRENDA website.

Each block:
    - starts with ID{tab}{e.c.}
    - copyright notice
    - information fields:
        - start with full name
        - short name{tab}{entry}
        - ends with 1+ newlines
    - ends with '///'.

References within entries:
    - #...# indicates a protein reference
    - <...> indicates a literature reference
    - (...) indicates a comment
    - {...} indicates field specific information.

Note that:
    - Compounds names can contain:
        - (multiple nested) {[()]}
        - Spaces (usually nested inside parentheses)
        - arrows '->' (complex sugars)
    - Reaction entries might have a reaction formula:
        - One or more entries separated by a '+'
        - Sides are separated by '='
        - Stoichiometry is optional (Presumably defaults to 1)
    - Comments can also have metabolite names and reactions inside and all
        associated characters.
    - Numerical values are denoted as:
        - single value
        - a range of values separated by a '-'
    - Not all entries seem to be unique.
    - Additional (low quality data) values can be saved under a 'more' entry.
        These entries will have -999 as value if required by the category.

List of short entry name:
    AC   - activating compound
    AP   - application
    CF   - cofactor
    CL   - cloned
    CR   - crystallization
    EN   - engineering
    EXP  - expression (Not seen in data)
    GI   - general information on enzyme (Not seen in data)
    GS   - general stability
    IC50 - IC-50 Value substrate in {...} (Not mentioned in README)
    ID   - EC-class (Not seen in data, only for the headers)
    IN   - inhibitors
    KKM  - Kcat/KM-Value substrate in {...} (Not seen in data)
    KI   - Ki-value    inhibitor in {...}
    KM   - KM-value    substrate in {...}
    LO   - localization
    ME   - metals/ions
    MW   - molecular weight
    NSP  - natural substrates/products reversibility information in {...}
    OS   - oxygen stability
    OSS  - organic solvent stability
    PHO  - pH-optimum
    PHR  - pH-range
    PHS  - pH stability
    PI   - isoelectric point
    PM   - posttranslation modification
    PR   - protein
    PU   - purification
    RE   - reaction catalyzed
    RF   - references  pubmed in {...} (Not mentioned in README)
    REN  - renatured
    RN   - accepted name (IUPAC)
    RT   - reaction type
    SA   - specific activity
    SN   - synonyms
    SP   - substrates/products reversibility information in {...}
    SS   - storage stability
    ST   - source/tissue
    SU   - subunits
    SY   - systematic name
    TN   - turnover number substrate in {...}
    TO   - temperature optimum
    TR   - temperature range
    TS   - temperature stability
"""
import pathlib
import re
import warnings
import itertools
import collections
import json
import sqlite3
import os

# Entry headers
ENTRY_HEADERS = {
    "AC": "ACTIVATING_COMPOUND",
    "AP": "APPLICATION",
    "CF": "COFACTOR",
    "CL": "CLONED",
    "CR": "CRYSTALLIZATION",
    "EN": "ENGINEERING",
    # "EXP": "EXPRESSION", # Doesn't seem to exist in the data
    # "GI": "GENERAL_INFO", # Doesn't seem to exist in the data
    "GS": "GENERAL_STABILITY",
    "IC50": "IC50_VALUE",
    # "ID": "ID", # Already matched for the EC section start
    "IN": "INHIBITORS",
    # "KKM": "KKM_VALUE", # Doesn't seem to exists in the data.
    "KI": "KI_VALUE",
    "KM": "KM_VALUE",
    "LO": "LOCALIZATION",
    "ME": "METALS_IONS",
    "MW": "MOLECULAR_WEIGHT",
    "NSP": "NATURAL_SUBSTRATE_PRODUCT",
    "OS": "ORGANIC_SOLVENT_STABILITY",
    "OSS": "OXIDATION_STABILITY",
    "PHO": "PH_OPTIMUM",
    "PHR": "PH_RANGE",
    "PHS": "PH_STABILITY",
    "PI": "PI_VALUE",
    "PM": "POSTTRANSLATIONAL_MODIFICATION",
    "PR": "PROTEIN",
    "PU": "PURIFICATION",
    "RE": "REACTION",
    "RF": "REACTION_TYPE",
    "REN": "RECOMMENDED_NAME",
    "RN": "REFERENCE",
    "RT": "RENATURED",
    "SA": "SOURCE_TISSUE",
    "SN": "SPECIFIC_ACTIVITY",
    "SP": "STORAGE_STABILITY",
    "SS": "SUBSTRATE_PRODUCT",
    "ST": "SUBUNITS",
    "SU": "SYNONYMS",
    "SY": "SYSTEMATIC_NAME",
    "TN": "TEMPERATURE_OPTIMUM",
    "TO": "TEMPERATURE_RANGE",
    "TR": "TEMPERATURE_STABILITY",
    "TS": "TURNOVER_NUMBER",
    }
SHORT_NAME_REGEX = re.compile('|'.join('(?:^({})\t)'.format(i)
                                       for i in sorted(ENTRY_HEADERS.keys())))
FULL_NAME_REGEX = re.compile('|'.join('(?:^({})$)'.format(i)
                                      for i in sorted(ENTRY_HEADERS.values())))

PARSE_SPECIFIC_FIELD = {'IC50', 'KKM', 'KI', 'KM', 'NSP', 'SP', 'TN', 'RF'}
PARSE_COMMENT_WTH_PROT_ONLY = {'AC', 'IN', 'CF'}

# Brenda references
PROT_REF_REGEX = re.compile(r'#([\d,\s]+)#')
LIT_REF_REGEX = re.compile(r'<([\d,\s]+)>')
COMMENT_REF_REGEX = re.compile(r'\((.+)\)', re.DOTALL)
COMMENT_STRICT_REF_REGEX = re.compile(r'(?:\A|\s)\((.+)\)(?:\s|\Z)', re.DOTALL)
COMMENT_STRICT_WITH_PROT_REF_REGEX = re.compile(r'(?:\A|\s)\((#.+)\)(?:\s|\Z)', re.DOTALL)
SPECIFIC_FIELD_REGEX = re.compile(r'\{(.+)\}', re.DOTALL)
SPECIFIC_FIELD_STRICT_REGEX = re.compile(r'(?:\A|\s)\{(.+)\}(?:\s|\Z)', re.DOTALL)
REACTION_END_REGEX = re.compile(r'(?:.*\s\+)?\s[^\s]+?(\Z|\s[|\({<])(?!.*=)|more = \?(\Z|\s[|\({<])')

# Parser regex
STARTING_PROT_REF_REGEX = re.compile(r'^#[,\d\s]+#(?:\Z|\s)+')
ENDING_COMMENT_REGEX = re.compile(r'(?:\A|\s)+<[,\d\s]+>$')
PH_REGEX = re.compile(r'pH\s(\d+\.?\d*)')
TEMP_REGEX = re.compile(r'(\d+)°C')
# EC regex taken from identifiers.org
EC_REGEX = re.compile(r'\d+\.-\.-\.-|\d+\.\d+\.-\.-|\d+\.\d+\.\d+\.-|\d+\.\d+\.\d+\.(n)?\d+')
PUBMED_REGEX = re.compile(r'Pubmed:(\d+)')
UNIPROT_REGEX = re.compile(r'(\w+)\sUniProt')
SWISSPROT_REGEX = re.compile(r'(\w+)\sSwissProt')
# Uniprot regex taken from uniprot website.
SECONDARY_UNIPROT_REGEX = re.compile(r'[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]'
                                     r'([A-Z][A-Z0-9]{2}[0-9]){1,2}')
STOICHIOMETRY_REGEX = re.compile(r'(^\d*\s)?(.+)')
WILD_TYPE_REGEX = re.compile(r'wild[\s-]type', re.IGNORECASE)
MUTANT_REGEX = re.compile(r'mutant', re.IGNORECASE)
TEMP_RANGE_REGEX = re.compile(r'(-?\d+)-(-?\d+)')


def nested_split(matches, sep=re.compile(r'[\s,]')):
    return list(itertools.chain.from_iterable(re.split(sep, i) for i in matches))


def scan(text, end0, end1, closers=']})>', openers='[{(<', not_nestable='|#',
         skiparrows=True):
    """Scan text, returning the position of a character in end0 followed by a
    character in end1, but only when not surrounded by any grouping character.
    """
    openers = {j: i for i, j in enumerate(openers)}
    closers = {j: i for i, j in enumerate(closers)}
    not_nestable = {j: i for i, j in enumerate(not_nestable, start=len(openers))}
    # Create a stack to keep track of the nested level.
    stack = [0] * (len(openers) + len(not_nestable))
    for idx, c in enumerate(text):
        if (skiparrows and c in '<>' and
                len(text) > (idx + 1) and idx > 0 and
                ('-' in (text[idx-1], text[idx+1]))):
            continue
        elif c in openers:
            stack[openers[c]] += 1
        elif c in closers:
            stack[closers[c]] -= 1
        elif c in not_nestable:
            # We just assume here that every 1st one opens and every 2nd one closes
            # since you cannot differentiate anyway.
            stack[not_nestable[c]] = (stack[not_nestable[c]] + 1) % 2
        # Use absolute for reverse scan where we see the closer first.
        if sum(abs(i) for i in stack) == 0 and c in end0 and text[idx+1] in end1:
            return idx
    return idx


class EmptyEntryException(ValueError):
    pass


class Parser():
    def __init__(self, debug=False):
        self.debug = debug

    def parse(self, entry, ec_number):
        try:
            short_name, entry = entry.split('\t', maxsplit=1)
        except ValueError:
            raise EmptyEntryException
        prot = sorted([int(i) for i in set(nested_split(PROT_REF_REGEX.findall(entry))) if i])
        lit = sorted([int(i) for i in set(nested_split(LIT_REF_REGEX.findall(entry))) if i])

        # Not all categories have a specific field, but they might have curly braces in the
        # comment field, so better to skip if not required.
        if short_name in PARSE_SPECIFIC_FIELD:
            specific_field = SPECIFIC_FIELD_STRICT_REGEX.search(entry)
        else:
            specific_field = None

        # Start looking for comment only after specific fields are found, as
        # specific fields might have parentheses inside.
        if specific_field:
            start_comment = specific_field.span(1)[-1] - 1
            specific_field = specific_field.group(1)
        else:
            start_comment = 0

        # We start checking the comment only after the specific field
        if short_name in PARSE_COMMENT_WTH_PROT_ONLY:
            comment = COMMENT_STRICT_WITH_PROT_REF_REGEX.search(entry[start_comment:])
        else:
            comment = COMMENT_STRICT_REF_REGEX.search(entry[start_comment:])
        if comment:
            comment = comment.group(1)

        parsed_entry = {
            'ec_number': ec_number,
            'short_name': short_name,
            'full_text': entry,
            'protein_ref': prot,
            'literature_ref': lit,
            'comment': comment,
            'specific_field': specific_field,
            }

        f_name = '_parse_{}'.format(short_name)
        if f_name in dir(self):
            f = getattr(self, f_name)
            try:
                parsed_entry = f(parsed_entry)
            except Exception:
                print(entry)
                print(parsed_entry)
                raise
        else:
            raise ValueError("No parsers for {}.".format(short_name))

        if not self.debug:
            # These should be fully processed, so can be removed to safe space.
            del parsed_entry['full_text']
            del parsed_entry['short_name']
            del parsed_entry['specific_field']
        return parsed_entry

    def remove_comment_from_full_text(self, text, comment):
        if comment:
            text = text.replace('({})'.format(comment), '')
        return text

    def remove_specific_field_from_full_text(self, text, specific_field):
        if specific_field:
            text = text.replace('{{{}}}'.format(specific_field), '')
        return text

    def remove_start_protein_reference(self, text):
        return STARTING_PROT_REF_REGEX.sub('', text)

    def remove_end_lit_reference(self, text):
        return ENDING_COMMENT_REGEX.sub('', text)

    def get_temp_ph(self, text):
        if text is None:
            return None, None
        temp = TEMP_REGEX.search(text)
        if temp:
            temp = float(temp.group(1))
        ph = PH_REGEX.search(text)
        if ph:
            ph = float(ph.group(1))
        return temp, ph

    def get_pubmed_id(self, text):
        if text is None:
            return None
        pubmed = PUBMED_REGEX.search(text)
        if pubmed:
            pubmed = pubmed.group(1)
        return pubmed

    def get_reversibility(self, text):
        if '{ir}' in text:
            return False
        elif '{r}' in text:
            return True
        return None

    def get_range(self, text):
        match = TEMP_RANGE_REGEX.search(text)
        return float(match.group(1)), float(match.group(2))

    def get_single_or_range(self, text, entry, key):
        try:
            entry[key] = float(text)
            entry[key + '_min'] = None
            entry[key + '_max'] = None
        except ValueError:
            mi, ma = self.get_range(text)
            entry[key] = None
            entry[key + '_min'] = mi
            entry[key + '_max'] = ma
        return entry

    def clean(self, entry):
        text = self.remove_comment_from_full_text(entry['full_text'], entry['comment'])
        text = self.remove_specific_field_from_full_text(text, entry['specific_field'])
        text = self.remove_start_protein_reference(text)
        text = self.remove_end_lit_reference(text)
        return text.strip()

    def get_stoichiometry(self, text):
        match = STOICHIOMETRY_REGEX.search(text)
        if match is None:
            return None, None
        else:
            n = 1 if match.group(1) is None else match.group(1)
            return n, match.group(2)

    def parse_reaction_string(self, text):
        """Separate the reaction and the comment.

        In contrast to most other entries, parsing the reaction is rather more complicated, as
        metabolites can have both spaces and parenthesis in their name like the text in the comments.

        Furthermore, the comment itself can contain + and = as well.

        This makes it non-trivial to parse using regex, so we use a simple stack based parser.
        We start at the end, to discern the comment, stopping when we see a + or = that is not
        nested within any other operator.
        Then we do a forward scan from this point on to find the start of the actual comment.

        One thing it will not parse is the following examples:
        A + B = C + a (a)a (actual comment)
        A + B = C + a (a)a
        where a (a)a a is supposed to be one metabolite name...
        Also can't handle unpaired -> in the reaction name of some complex sugar complexes.
        """

        # First do a backward scan: Either to the first + or = of the reaction
        rev_text = text[::-1]
        idx = scan(rev_text, '+=', ' ')
        # Don't forget to flip the index, since we have the reverse index!
        last_reaction_bit = text[-idx+1:]
        # Now do a forward scan to determine the end of the last metabolite.
        # Denoted by the start of a lit reference '<', comment '#', specific field '{' or protein '#'.
        # Additionally, this field has an extra comment field (?) wrapped in | ... |
        rxn_end = scan(last_reaction_bit, ' ', '<({#|')

        # Recombine the reaction parts
        reaction = text[:-idx+1] + last_reaction_bit[:rxn_end+1]
        # All the rest is the comment.
        comment = last_reaction_bit[rxn_end+1:]
        return reaction.strip(), comment.strip()

    def parse_reaction(self, entry):
        text = entry['full_text']
        text = self.remove_start_protein_reference(text)
        text = self.remove_end_lit_reference(text)
        reaction_text, other_text = self.parse_reaction_string(text)

        # Re-parse the comment and specific fields
        comment = COMMENT_REF_REGEX.search(other_text)
        if comment:
            comment = comment.group(1)
        specific_field = SPECIFIC_FIELD_REGEX.search(other_text)
        if specific_field:
            specific_field = specific_field.group(1)

        try:
            reactants, products = (i.split(' + ') for i in reaction_text.split('='))
        except ValueError:
            print(entry['full_text'])
            warnings.warn("Could not split entry as reaction: {}".format(reaction_text))
            reactants = None
            products = None
        else:
            reactants = [self.get_stoichiometry(i.strip()) for i in reactants]
            products = [self.get_stoichiometry(i.strip()) for i in products]

        entry['comment'] = comment
        entry['specific_field'] = specific_field
        entry['reactants'] = reactants
        entry['products'] = products
        entry['reversible'] = self.get_reversibility(entry['full_text'])
        entry['reaction'] = 'reaction_text'
        return entry

    def get_wild_type(self, text):
        if text is None:
            return None
        elif WILD_TYPE_REGEX.search(text):
            return True
        else:
            return False

    def get_mutant(self, text):
        if text is None:
            return None
        elif MUTANT_REGEX.search(text):
            return True
        else:
            return False

    def _parse_AC(self, entry):
        entry['activator'] = self.clean(entry)
        return entry

    def _parse_AP(self, entry):
        entry['application'] = self.clean(entry)
        return entry

    def _parse_CF(self, entry):
        entry['cofactor'] = self.clean(entry)
        return entry

    def _parse_CL(self, entry):
        entry['cloned'] = self.clean(entry)
        return entry

    def _parse_CR(self, entry):
        entry['crystallization'] = self.clean(entry)
        return entry

    def _parse_EN(self, entry):
        entry['engineering'] = self.clean(entry)
        return entry

    def _parse_GS(self, entry):
        entry['general_stability'] = self.clean(entry)
        return entry

    def _parse_IC50(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'ic50')
        entry['metabolite'] = entry['specific_field'] if entry['specific_field'] else None
        entry['wild_type'] = self.get_wild_type(entry['comment'])
        entry['mutant'] = self.get_mutant(entry['comment'])
        return entry

    def _parse_IN(self, entry):
        entry['inhibitor'] = self.clean(entry)
        return entry

    def _parse_KI(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'ki')
        entry['temperature'], entry['ph'] = self.get_temp_ph(entry['comment'])
        entry['metabolite'] = entry['specific_field'] if entry['specific_field'] else None
        entry['wild_type'] = self.get_wild_type(entry['comment'])
        entry['mutant'] = self.get_mutant(entry['comment'])
        return entry

    def _parse_KM(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'km')
        entry['temperature'], entry['ph'] = self.get_temp_ph(entry['comment'])
        entry['metabolite'] = entry['specific_field'] if entry['specific_field'] else None
        entry['wild_type'] = self.get_wild_type(entry['comment'])
        entry['mutant'] = self.get_mutant(entry['comment'])
        return entry

    def _parse_LO(self, entry):
        entry['localization'] = self.clean(entry)
        return entry

    def _parse_ME(self, entry):
        entry['metal_ions'] = self.clean(entry)
        return entry

    def _parse_MW(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'molecular_weight')
        return entry

    def _parse_NSP(self, entry):
        entry = self.parse_reaction(entry)
        return entry

    def _parse_OS(self, entry):
        # Not very useful
        entry['oxidation_stability'] = self.clean(entry)
        return entry

    def _parse_OSS(self, entry):
        entry['solvent'] = self.clean(entry)
        return entry

    def _parse_PHO(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'ph_optimum')
        return entry

    def _parse_PHR(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'ph_range')
        return entry

    def _parse_PHS(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'ph_stability')
        return entry

    def _parse_PI(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'pi')
        return entry

    def _parse_PM(self, entry):
        entry['posttranslational_modification'] = self.clean(entry)
        return entry

    def _parse_PR(self, entry):
        if len(entry['protein_ref']) > 1:
            raise ValueError("Multiple protein references in one definition.")
        entry['protein_ref'] = entry['protein_ref'][0]
        entry['organism'] = self.clean(entry)
        uniprot = UNIPROT_REGEX.search(entry['organism'])
        if uniprot is not None:
            entry['uniprot'] = uniprot.group(1)
            entry['organism'] = entry['organism'].replace(uniprot.group(0), '').strip()
        else:
            entry['uniprot'] = None
        swissprot = SWISSPROT_REGEX.search(entry['organism'])
        if swissprot is not None:
            entry['swissprot'] = swissprot.group(1)
            entry['organism'] = entry['organism'].replace(swissprot.group(0), '').strip()
        else:
            entry['swissprot'] = None
        if uniprot is None and swissprot is None:
            # Try the uniprot regex without the Uniprot marker.
            uniprot = SECONDARY_UNIPROT_REGEX.search(entry['organism'])
            if uniprot is not None:
                entry['uniprot'] = uniprot.group(0)
                entry['organism'] = entry['organism'].replace(uniprot.group(0), '').strip()
        return entry

    def _parse_PU(self, entry):
        entry['purification'] = self.clean(entry)
        return entry

    def _parse_RE(self, entry):
        entry = self.parse_reaction(entry)
        return entry

    def _parse_RF(self, entry):
        if len(entry['literature_ref']) > 1:
            raise ValueError("Multiple protein references in one definition.")
        entry['literature_ref'] = entry['literature_ref'][0]
        # Reference has different format, so don't use clean.
        # In addition, it might have more text in parenthesis, so the comment is not correct.
        # for this field
        reference = entry['full_text'].replace('{{{}}}'.format(entry['specific_field']), '')
        entry['reference'] = LIT_REF_REGEX.sub('', reference).strip()
        entry['pubmed_id'] = self.get_pubmed_id(entry['specific_field'])
        entry['comment'] = None
        return entry

    def _parse_REN(self, entry):
        entry['renatured'] = self.clean(entry)
        return entry

    def _parse_RN(self, entry):
        entry['recommended_name'] = entry['full_text'].strip()
        return entry

    def _parse_RT(self, entry):
        entry['reaction_type'] = entry['full_text'].strip()
        return entry

    def _parse_SA(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'specific_activity')
        entry['temperature'], entry['ph'] = self.get_temp_ph(entry['comment'])
        entry['metabolite'] = entry['specific_field'] if entry['specific_field'] else None
        entry['wild_type'] = self.get_wild_type(entry['comment'])
        entry['mutant'] = self.get_mutant(entry['comment'])
        return entry

    def _parse_SN(self, entry):
        entry['systematic_name'] = entry['full_text'].strip()
        return entry

    def _parse_SP(self, entry):
        entry = self.parse_reaction(entry)
        return entry

    def _parse_SS(self, entry):
        entry['storage_stability'] = self.clean(entry)
        return entry

    def _parse_ST(self, entry):
        entry['source_tissue'] = self.clean(entry)
        return entry

    def _parse_SU(self, entry):
        entry['subunits'] = self.clean(entry)
        return entry

    def _parse_SY(self, entry):
        entry['synonym'] = self.clean(entry)
        return entry

    def _parse_TN(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'turnover_number')
        entry['temperature'], entry['ph'] = self.get_temp_ph(entry['comment'])
        entry['metabolite'] = entry['specific_field'] if entry['specific_field'] else None
        entry['wild_type'] = self.get_wild_type(entry['comment'])
        entry['mutant'] = self.get_mutant(entry['comment'])
        return entry

    def _parse_TO(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'temperature_optimum')
        return entry

    def _parse_TR(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'temperature_range')
        return entry

    def _parse_TS(self, entry):
        entry = self.get_single_or_range(self.clean(entry), entry, 'temperature_stability')
        return entry


def parse_section(parser, lines, ec_number):
    # Merge lines that are not full entries.
    entry = []
    for line in lines:
        match = SHORT_NAME_REGEX.match(line)
        # New entry, yield and clear entry.
        if match is not None and entry:
            try:
                yield parser.parse((' '.join(entry)), ec_number)
            except EmptyEntryException:
                pass
            except Exception as ex:
                with open('failures.txt', 'a') as f:
                    f.write('{}\n'.format(repr(ex)))
                    f.write('ID\t{}\n'.format(ec_number))
                    f.writelines(entry)
                    # Don't forget to reset the entry if we just failed to parse!
                    entry = []
                continue
            entry = [line]
        # No new entry, so append only.
        else:
            entry.append(line)
    try:
        yield parser.parse((' '.join(entry)), ec_number)
    except EmptyEntryException:
        pass


def parse_ec_block(parser, lines, ec_number):
    section_lines = []
    section_type = None
    for line in lines:
        match = FULL_NAME_REGEX.match(line)
        if match is not None:
            if section_type is not None:
                yield section_type, parse_section(parser, section_lines, ec_number)
            section_type = match.group(0)
            section_lines = []
        else:
            section_lines.append(line)
    yield section_type, parse_section(parser, section_lines, ec_number)


def parse_brenda(brenda_file_path, debug=False):
    parser = Parser(debug=debug)
    with open(brenda_file_path, 'r') as brenda_file:
        ec_block = []
        ec_number = None
        for i, line in enumerate(brenda_file):
            # Skip copyright block.
            if line.startswith('*'):
                continue
            line = line.strip()
            # Skip lines empty after stripping white spaces.
            if not line:
                continue
            # Save EC once we find it
            elif line.startswith('ID\t'):
                if ec_number is not None:
                    warnings.warn("Double EC-number entry at line: {}".format(i))
                match = EC_REGEX.search(line)
                if match is None:
                    warnings.warn("Found ID entry with no regex match: {} - {}".format(i, line))
                ec_number = match.group()
            # Once we find the closing sequence, we are done with a block
            elif line.startswith('///'):
                yield ec_number, parse_ec_block(parser, ec_block, ec_number)
                ec_block = []
                ec_number = None
            else:
                ec_block.append(line)
    if ec_number is not None or ec_block:
        warnings.warn("Unfinished block: {}\n{}".format(ec_number, '\n'.join(ec_block)))


def initialize_sql_tables(conn, block, debug=False):
    c = conn.cursor()
    # Create literature reference table
    c.execute("""CREATE TABLE literature (
                    ec text,
                    id integer,
                    reference text,
                    pubmed text,
                    comment text
                    )
              """)
    c.execute("""CREATE INDEX ec_ref_id_index ON literature(ec, id)""")
    # Create protein reference table
    c.execute("""CREATE TABLE protein (
                    ec text,
                    id integer,
                    organism text,
                    pubmed text,
                    uniprot text,
                    swissprot text,
                    comment text
                    )
            """)
    c.execute("""CREATE INDEX ec_prot_id_index ON protein(ec, id)""")
    # Create protein to literature reference table.
    c.execute("""CREATE TABLE protein_ref (
                    ec text,
                    protein_id integer,
                    ref_id integer
                    )
             """)

    skip = {'full_text', 'short_name', 'specific_field',
            'literature_ref', 'protein_ref'}
    if debug:
        skip.remove('full_text')
    real = {'temperature', 'temperature_stability', 'temperature_range', 'temperature_optimum',
            'ph', 'ph_stability', 'ph_optimum', 'ph_range',
            'ki', 'km', 'specific_activity', 'turnover_number',
            'pi', 'IC50', 'molecular_weight',
            }
    real.update({i + '_min' for i in real}, {i + '_max' for i in real})
    integer = {'wild_type', 'mutant', 'reversible'}

    template = """CREATE TABLE {table_name} (
                    entry integer,
                    {columns}
                      )
               """

    template_ref = """CREATE TABLE {table_name}_ref (
                          ec text,
                          entry integer,
                          ref_id integer
                          )
                   """

    template_prot = """CREATE TABLE {table_name}_prot (
                           ec text,
                           entry integer,
                           prot_id integer
                           )
                    """

    tables = []
    for table in block.keys():
        if table in ('PROTEIN', 'REFERENCE'):
            continue
        else:
            table_name = table.lower()
            fields = []
            for k, v in block[table][0].items():
                if k in skip:
                    continue
                elif isinstance(v, list):
                    continue
                elif k in real:
                    v_t = 'real'
                elif k in integer:
                    v_t = 'integer'
                else:
                    v_t = 'text'
                fields.append((k, v_t))
            tables.append((table_name, fields))
            columns = ',\n'.join([' '.join(i) for i in fields])
            c.execute(template.format(columns=columns, table_name=table_name))
            c.execute(template_ref.format(table_name=table_name))
            c.execute(template_prot.format(table_name=table_name))
    return tables


def read_in_sql_data(conn, block, tables):
    c = conn.cursor()
    # Load references, and protein references first.
    if 'REFERENCE' in block:
        c.executemany("""INSERT INTO literature (
                             ec, id, reference, pubmed, comment)
                             values (
                             :ec_number, :literature_ref, :reference, :pubmed_id, :comment)
                     """, block['REFERENCE'])

    if 'PROTEIN' in block:
        c.executemany("""INSERT INTO protein (
                             ec, id, organism, uniprot, swissprot, comment)
                             values (
                             :ec_number, :protein_ref, :organism,
                             :uniprot, :swissprot, :comment)
                     """, block['PROTEIN'])

        # Next we load the protein reference to literature reference data.
        entries = [(i['ec_number'], i['protein_ref'], j)
                   for i in block['PROTEIN']
                   for j in i['literature_ref']]
        c.executemany("""INSERT INTO protein_ref (
                             ec, protein_id, ref_id)
                             values (?, ?, ?)
                      """, entries)

    template = """INSERT INTO {table_name} (
                      entry, {field_names})
                      values (:entry, {values})
               """

    template_ref = """INSERT INTO {table_name}_ref (
                          ec, entry, ref_id)
                          values (?, ?, ?)
                   """

    template_prot = """INSERT INTO {table_name}_prot (
                          ec, entry, prot_id)
                          values (?, ?, ?)
                   """
    # Finally, for each category, load the data first, then load the
    # references and protein references afterwards.
    for table, fields in tables:
        try:
            data = block[table.upper()]
        except KeyError:
            # No data for this category in this ec number
            continue
        [i.update({'entry': idx}) for idx, i in enumerate(data)]
        field_names = ', '.join([i[0] for i in fields])
        value_placeholders = ', '.join(':' + i[0] for i in fields)
        t = template.format(table_name=table, field_names=field_names,
                            values=value_placeholders)
        c.executemany(t, data)

        ref_entries = [(i['ec_number'], idx, j)
                       for idx, i in enumerate(data)
                       for j in i['literature_ref']]
        c.executemany(template_ref.format(table_name=table), ref_entries)

        prot_entries = [(i['ec_number'], idx, j)
                        for idx, i in enumerate(data)
                        for j in i['protein_ref']]
        c.executemany(template_prot.format(table_name=table), prot_entries)


# Notes:
# Some reactions are saved as actual reaction formula's, some as descriptions
# (especially more general enzymes). These will fail to parse and give a warning.
# Note that failures to parse will be written to 'failures.txt' in the current directory.
# Currently no entries fail to parse. (Although more might be parsed incorrectly...)
# In addition, note that while I separate the reaction species and save the stoichiometry
# in the json output, I did not save them separately to the sqllite database yet.
# It would best be represented as the following tables:
# 1) Metabolite name to reference table
# 2) Metabolite reference to reference reaction (+ stoichiometry) table
# 3) Add a reference to the metabolite table where appropriate (parameters, inhibitors etc.)
# Finally, note that each entry might have saved extra information under the name "more"
# This generally contains more references and notes where the value could not be used
# in the BRENDA format, but that might be relevant nonetheless.


if __name__ == "__main__":
    test = False

    # Actually load all the data
    if not test:
        # You can change the input and output path here if needed.
        brenda_file_path = pathlib.Path('../../../data/BRENDA-db/brenda_download.txt')
        database_path = pathlib.Path('../../../data/BRENDA-db/brenda.db')

        if os.path.exists(database_path):
            os.remove(database_path)
        conn = sqlite3.connect(database_path)

        brenda_iterator = parse_brenda(brenda_file_path, debug=False)

        # Chunk it per EC block.
        initialized = False
        for ec_number, brenda_block in brenda_iterator:
            block = collections.defaultdict(list)
            for section_type, section in brenda_block:
                for entry in section:
                    block[section_type].append(entry)
            block = dict(block)

            if not initialized:
                initialized = True
                with conn:
                    tables = initialize_sql_tables(conn, block)

            with conn:
                read_in_sql_data(conn, block, tables)

    # Test version where we load just one ec-number for quick testing.
    else:
        parse_ec = '1.1.1.14'
        brenda_file_path = pathlib.Path('../../../data/BRENDA-db/brenda_download.txt')
        brenda = parse_brenda(brenda_file_path, debug=True)

        # Thanks to the lazy parsing, it is quite quick to just search for the ec wanted.
        for ec_number, brenda_block in brenda:
            if ec_number != parse_ec:
                pass
            else:
                break

        block = collections.defaultdict(list)
        for section_type, section in brenda_block:
            for entry in section:
                block[section_type].append(entry)
        block = dict(block)

        # Test to save as json
        with open('test.json', 'w') as outfile:
            json.dump(block, outfile, indent=2)

        # Test to save as sql
        if os.path.exists('test.db'):
            os.remove('test.db')
        conn = sqlite3.connect('test.db')

        with conn:
            tables = initialize_sql_tables(conn, block)

        with conn:
            read_in_sql_data(conn, block, tables)
