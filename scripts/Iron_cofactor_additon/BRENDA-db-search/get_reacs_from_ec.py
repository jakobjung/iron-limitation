"""
Title:      Querying a database for enzymes with iron-containing cofactors.
Author:     Jakob Jung
Date:       08 November 2018
Details:    Takes data from the file all_ecs_from_model_validated.csv, which consists of EC-numbers
            referring to iron-related reactions. These reactions can be
"""

import csv
import cobra


def get_ecs_model(model):
    """Returns dictionary of all reactions (val) and their ec-numbers (key).

    :param model: Metabolic model which is previously downloaded and read (obj)
    :return: dict. vals: reactions containing ec-numbers, keys: ec-numbers
             (both are strings).
    """
    dict_ec = {}  # initialize dictionary

    # Create dict. with reac-ID as keys and EC-number(s) as values, if
    # there are EC numbers available:
    for reac in model.reactions:
        if 'EC' in reac.notes.keys():
            ec_list = reac.notes['EC'].split(';')  # Because if there ae more ecs involved, ";" separates
            for e in ec_list:
                if e in dict_ec.keys():
                    dict_ec[e] += [reac.id]
                else:
                    dict_ec[e] = [reac.id]
    return dict_ec      # like: {'1.1.1.1': ['GPD1','GPD3'], if one enz has 2 reactions


def find_enzymes(model, enzlist):
    """returns lists of ec-number and reaction ids of matches in model.

    Uses the get_ecs_model function for finding all ECs first, then finds matches

    :param model: Metabolic model which is previously downloaded and read (obj)
    :param enzlist: list of ec-numbers (strings), which are searched for in
                    model
    :return: list of IDs of reactions (str) which are related to the enzymes
             from the input list.
    """
    d_e = get_ecs_model (model)     # gets ec-nrs + their ids of whole model in a dict.
    l_e = enzlist                   # enzymes (ec) to be compared to model's ec's. ['1.2.1.1',...]
    d_e_keys = list(d_e.keys())     # get keys (Ecs) FROM DICT OF reaction ecs
    all_ecs = []
    res_list = []
    ec_list = []
    # create list of all ecs
    for ec in l_e:
        for k in d_e_keys:
            if ec == k:
                if d_e[k] not in res_list:
                    res_list += d_e[k]  # if you want ids, put '+= [ec]'
                if ec not in ec_list:   # can be left-out to see all reactions!
                    ec_list += [ec]
    return res_list, ec_list






if __name__ == '__main__':
    # get csv values:
    tot_ec = []
    with open("../../../data/BRENDA-db/all_ecs_from_model_validated.csv") as file:
        r = csv.reader(file)
        for ent in r:
            for col in ent:
                tot_ec += [col]
    saureus = cobra.io.load_json_model("../../../data/models-sa/curated-models/\
SA_N315_uid57837_158879_1.json")
    res_list, ec_list = find_enzymes(saureus, tot_ec)
    # test, how many were found:
    print(ec_list[:20])
    print(res_list[:20])
    print (len(ec_list))
    print(len(res_list))





