#BRENDA-db-search
These scripts are written to find all iron related enzymes of the _S. aureus_ N
315 model.
## brenda-parser.py
parser, written by Rik. It can be used to convert the downloaded text-file of
the brenda database into a database in SQL format.

downloaded from Gitlab (01-November-2018):
https://gitlab.com/wurssb/brenda-parser/blob/master/brenda.py

## get_reacs_from_ec.py:
Script which connects all iron-related ec-numbers found in the SQL-query
with the reactions in the model. The output should be a list of the 
reactions, which are related to iron-requiring enzymes

## query_ecs_sqlite.sql
SQLite query, which has been applied to find out all EC-numbers of enzymes
connected to iron. 

Attention! The final csv ("data/BRENDA-db/all_ecs_from_model_validated.csv")
is curated manually, so the query cannot be reproduced by the code!
