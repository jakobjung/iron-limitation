SELECT DISTINCT ec
FROM protein P 
JOIN cofactor C ON P.ec = C.ec_number
JOIN activating_compound A ON A.ec_number = P.ec
JOIN metals_ions M ON M.ec_number = A.ec_number
WHERE  (C.comment REGEXP ' [iI]ron|heme|2Fe-2S|2Fe-4S|FeS' 
OR A.comment REGEXP ' [iI]ron|hem[eo]|2Fe-2S|2Fe-4S|FeS' 
OR M.comment REGEXP ' [iI]ron|hem[eo]|2Fe-2S|2Fe-4S|FeS'
OR C.cofactor REGEXP 'Fe|[^T][Ii]ron|hem[eo]|[fF]err' 
OR A.activator REGEXP 'Fe|[^T][Ii]ron|hem[eo]|[fF]err' 
OR M.metal_ions REGEXP 'Fe|[^T][Ii]ron|hem[eo]|[fF]err')
ORDER BY ec