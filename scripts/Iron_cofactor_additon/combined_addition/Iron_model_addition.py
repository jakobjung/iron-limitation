"""
Title:      Change Model reactions by adding iron to model
Author:     Jakob Jung
Date:       13 November 2018
Details:    Functions created to use the found iron-requiring reactions and
            add iron generation to those reactions. Reversible reactions are
            divided into 2 reactions. e.g. "A <--> B" is converted to:
            "A --> B + iron" and "B --> A + Iron". Additionally, an exchange-
            reaction of iron is added.

"""
from copy import deepcopy
import cobra
import csv
# import functions from different directories of project:
import sys
sys.path.insert(0, '../pfam-search')
import Pfam
sys.path.insert(0, '../BRENDA-db-search')
import get_reacs_from_ec


def add_fe (model, reaction_IDs):
    """Adds iron metabolite to specified reactions.

    :param model: Metabolic model which is previously downloaded and read (obj)
    :param reaction_IDs: IDs of reactions to be targeted
    :return: Modified model with added Fe-metabolites.reversible reactions are
             split and iron is added to specified reactions. Additionally, an
             iron-exchange reaction is added.
    """
    iron = cobra.Metabolite('Fe', formula='Fe', name='Iron (Cofactor)',
                            compartment='c')
    add_cof_exch(model, iron)

    for reac in reaction_IDs:   #go through reactions which need to add iron

        r = model.reactions.get_by_id(reac)
        new_r = deepcopy(r)

        if r.reversibility:     #check if reaction is reversible
            r.lower_bound = 0   #remove reversibility
            r.id += "_fw"       #add '_fw' to show it's the forward reaction
            new_r.upper_bound = 0
            new_r.id += "_rv"
            model.add_reaction(new_r)
            new_r.add_metabolites({iron: -1})
            r.add_metabolites({iron: 1}) #Add iron metabolites (backward reactions need to be -1):
        else:
            if r.upper_bound > 0:
                r.add_metabolites({iron: 1})
            elif r.lower_bound < 0:
                r.add_metabolites({iron: -1})
    return model


def add_cof_exch(model, cof):
    """Add a metabolite's exchange-reaction to model

    :param model: Metabolic model which is previously downloaded and read (obj)
    :param cof: Metabolite that needs to be added (Metabolite object)
    :return: model with added metabolite (obj)
    """
    exchange = cobra.Reaction('EX_%s_LPAREN_e_RPAREN_'%(cof.id))
    exchange.name = 'EX %s'%(cof.id)
    exchange.upper_bound = 1000
    exchange.lower_bound = -1000
    exchange.add_metabolites({cof: -1.0})
    model.add_reactions([exchange])

    return model




if __name__ == '__main__':
    saureus = cobra.io.load_json_model("../../../data/models-sa/final_model_N315/saureus_N315.json")
    print("Model optimized:", saureus.slim_optimize())

    tot_ec = []
    with open("../../../data/BRENDA-db/all_ecs_from_model_validated.csv") as file:
        r = csv.reader(file)
        for ent in r:
            for col in ent:
                tot_ec += [col]
        res_list, ec_list = get_reacs_from_ec.find_enzymes(saureus, tot_ec)
    res_list = list(set(res_list)) #remove all duplicates
    print(len(res_list))
    # res_list = []
    #get all genes from pfams related to iron:
    all_pfam_genes = Pfam.get_iron_rel_genes()
    #compare them to genes in model, get them (27)
    genes = []
    for gene in saureus.genes:
        for g in all_pfam_genes:
            if gene.id == g[0] or gene.id == g[1]:
                genes += [gene.id]
    #check genes for reactions:
    for g in genes:
        for r in saureus.genes.get_by_id(g).reactions:
            if r.id not in res_list:
                res_list += [r.id]
    #check reslist after iron-related pfam-search:
    print(len(res_list))
    add_fe(saureus, res_list)
    f = saureus.metabolites.get_by_id("Fe")

    print(len(f.reactions))
    print(len(saureus.reactions))
    cobra.io.save_json_model(saureus, "../../../data/models-sa/iron_adjusted_model/SAN315_with_iron.json")
    cobra.io.write_sbml_model(saureus, "../../../data/models-sa/iron_adjusted_model/SAN315_with_iron.xml")


saureus = cobra.io.load_json_model("../../../data/models-sa/iron_adjusted_model/SAN315_with_iron.json")
