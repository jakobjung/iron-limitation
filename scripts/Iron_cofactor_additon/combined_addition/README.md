## iron_model_addition
Script, which can be used in order to add iron limitation to the model. 
The way in which it is done: 

The function 'add_fe' of the script takes the GEM and a list of all reactions
to be modified. It creates a metabolite, called Fe and adds it to all reactions
as a product. Reversible reactions are previously separated into forward and 
backward reactions. Then an exchange reaction is added, and the modified
model returned.