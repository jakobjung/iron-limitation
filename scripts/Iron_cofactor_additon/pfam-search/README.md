# pfam-search
## Pfam.py
Python-script, which uses the files of the folder 'data/pfam-data' and the GEM of 
_S. aureus_ N315 to find out, which genes of the model are related to iron,
based on proteins with iron-related Pfam-domains. 

The function "get_iron_rel_genes()" can be applied to retrieve a list of all 
those genes. This function is then used in the '/simulations/iron_model_addition'
file to modify all iron related reactions.