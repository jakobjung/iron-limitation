"""
Title:      Getting iron-relatd genes from a provided file.
Author:     Jakob Jung
Date:       08 November 2018
Details:    1. parse through Genbank file, extract all PDB-names,  Gene names
               and protein ids.
            2. compare pdb and gene names to model genes
            3. get the reactions related to those genes
"""

import csv
import re


def find_iron_rel_pfams(file):
    """finds all pfams in file using a regular expression

    :param file: input file
    :return: list of all pfams
    """
    ipfams = []
    with open(file, "r") as myfile:
        match = re.compile(r"pf[\d]{5}",re.IGNORECASE)
        tabel = csv.reader(myfile)
        for row in tabel:
            for col in row:
                res = match.findall(col)
                if len(res) > 0:
                    for i in res:
                        if i not in ipfams:
                            ipfams += [i]

    return ipfams


def genbank_parser (gb_file):
    """Parses through genbank file to extract pdb,prot_id, orf_id of genes.

    :param gb_file: input file (as string), containing '.gb'-file of organism
    :return: all_genes: list of lists of pdb,prot_id, orf_id for each gene
    """
    re_prot_id = re.compile(r'/protein_id="(.*)"')
    re_pdb = re.compile(r'/gene="(.*)"')
    re_orf_id = re.compile(r'ORFID:(.{6})')

    with open(gb_file,'r') as gb:
        prev_pdb = ''
        row = []
        all_genes = []
        for line in gb:
            pid = re_prot_id.search(line)
            pdb = re_pdb.search(line)
            orf = re_orf_id.search(line)
            if pid != None:
                row += [pid.group(1)]
                if len(row) == 2:
                    row.insert(0,"")
                all_genes+=[row]
                row = []
            elif pdb != None:
                if pdb.group(1) != prev_pdb:
                    row = [pdb.group(1)]
                    prev_pdb = pdb.group(1)

            elif orf != None:
                row += [orf.group(1)]
        return all_genes


def get_pfam_pid_list (all_genes, pid_pfam_file, pos1 = 2, pos2 = 0, pos3 = 1):
    """returns all genes correlated to same prot_id of file.

    :param all_genes:       list of list of all genes of organism (this
                            case, SA N315)
    :param iron_pfam_file:  file with protein ids of all iron-related pfam
                            genes
    :param pos:             position in lol with prot_id, by default 2
    :return: fe_rel_gns:    list of lists of all genes, whose proteins
                            containing iron related domains
    """
    pfamt = []
    fe_rel_gns = []
    matcher = re.compile(r"[A-Z]{3}[0-9]{5}\.\d")
    matcher2 = re.compile(r"pf[\d]{5}", re.IGNORECASE)
    with open(pid_pfam_file) as f:
        pfamtable = csv.reader(f)
        for p in pfamtable:
            line = []
            pfams = matcher2.search(p[pos3])
            pid = matcher.search(p[pos2])
            if pfams != None and pid != None:
                line += [pfams.group(), pid.group()]
            if len(line) > 0:
                if line not in pfamt:
                    pfamt += [line]
    return pfamt


def get_iron_rel_genes():
    """gives orfid and/or pdb_id of iron related genes"""
    all_genes = []
    i_r_pds = []
    i_r_pfams = find_iron_rel_pfams("../../../data/pfam-data/Iron_rel_pfams_rosato_22_12_2016_off.csv")
    a_g = genbank_parser("../../../data/pfam-data/sequence.gb")
    dom = get_pfam_pid_list(a_g, '../../../data/pfam-data/query-result.csv')
    for d in dom:
        for p in i_r_pfams:
            if p == d[0]:
                if d[1] not in i_r_pds:
                    i_r_pds += [d[1]]
    for g in a_g:
        for pds in i_r_pds:
            if g[2] == pds:
                if g[:-1] not in all_genes:
                    all_genes += [g[:-1]]
    return all_genes


if __name__ == '__main__':
    i_r_pfams = find_iron_rel_pfams("../../../data/pfam-data/Iron_rel_pfams_rosato_22_12_2016_off.csv")
    print(len(i_r_pfams), i_r_pfams[:10])
    a_g = genbank_parser("../../../data/pfam-data/sequence.gb")
    print(len(a_g), a_g[:10])
    dom = get_pfam_pid_list (a_g, '../../../data/pfam-data/query-result.csv')
    print(len(dom), dom[:10])
    z = get_iron_rel_genes()
