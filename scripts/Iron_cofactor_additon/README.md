# Iron co-factor addition
The three included directories contain code, which lead to an addition of iron to the
GEM. 
## BRENDA-db-search
Includes code, which: 
1. Creates an SQL-database for the BRENDA enzymes database
2. Queries through the database, and finds all iron-related enzymes (EC-numbers)
## pfam-search
Includes code, which finds reactions, which have iron-related Pfam-domains
## combined addition
Inclides code, which finally adds all iron-related reactions to the GEM of _S. aureus_ 
N315.